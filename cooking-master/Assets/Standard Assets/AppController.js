#pragma strict

static var adMobCall : boolean = true;
static var adMobShow : boolean = true;  

static var  InterstitialCall : boolean  = false;
static var  InterstitialShow : boolean  = false;

static var  CacheVideo : boolean  = false;
static var  ShowVideo : boolean  = false;

static var  CacheCB : boolean  = false;
static var  ShowCB : boolean  = false;

static var  ShowAdmob : boolean  = false;

static var isSound : boolean = true;
static var isMusic : boolean = true;

public static var NoAds :boolean  = false;
public static var PurchaseDone :boolean  = false;

public static var isPurchased :int = 0;
public static var isDialogue :boolean  = false;

public static var IsFalse :boolean  = false;

public static var CallLogin :boolean  = false;

//characters
public static var Character1 :boolean  = false;
public static var Character2 :boolean  = false;
public static var Character3 :boolean  = false;
public static var Character4 :boolean  = false;
public static var Character5 :boolean  = false;
public static var Character6 :boolean  = false;

//positions
public static var position1 :boolean  = false;
public static var position2 :boolean  = false;
public static var position3 :boolean  = false;
public static var position4 :boolean  = false;

public static var firstPosition :float  = -4.62;
public static var secondPosition :float  = -7.51;
public static var thirdPosition :float  = -10.3;
public static var forthPosition :float  = -12.9;

//Pans for indication
public static var pan1 :boolean  = false;
public static var pan2 :boolean  = false;
public static var pan3 :boolean  = false;
public static var pan4 :boolean  = false;

//Plates for indication
public static var plate1 :boolean  = false;
public static var plate2 :boolean  = false;
public static var plate3 :boolean  = false;
public static var plate4 :boolean  = false;

//how many items are served
public static var ServedAtOne :int  = -1;
public static var ServedAtTwo :int  = -1;
public static var ServedAtThree :int  = -1;
public static var ServedAtFour :int  = -1;

//how many items are ordered
public static var OrderedAtOne :int  = 0;
public static var OrderedAtTwo :int  = 0;
public static var OrderedAtThree :int  = 0;
public static var OrderedAtFour :int  = 0;


//Allow to move when served
public static var move1 :boolean  = false;
public static var move2 :boolean  = false;
public static var move3 :boolean  = false;
public static var move4 :boolean  = false;

public static var Lives :int  = 3;
public static var GameOver :boolean  = false;
public static var IsPaused :boolean  = false;

public static var Scores :int  = 0;
public static var Levels :int  = 1;

public static var CurrentScore :int  = 0;
public static var HighScore :int  = 0;

function Start () {

}

function Update () {

}

static function ClickSound()
		{
			var ClickMusic : GameObject = GameObject.Find("ClickObj");
		    ClickMusic.GetComponent.<AudioSource>().Play();
		}

static function Reset()
		{
			IsFalse = false;
		    Character1 = false;
			Character2 = false;
			Character3 = false;
			Character4 = false;
			Character5 = false;
			Character6 = false;

			position1 = false;
			position2 = false;
			position3 = false;
			position4 = false;

			firstPosition  = -4.62;
			secondPosition  = -7.51;
			thirdPosition  = -10.3;
			forthPosition = -12.9;

			pan1  = false;
			pan2  = false;
			pan3  = false;
			pan4  = false;

			plate1  = false;
			plate2 = false;
			plate3 = false;
			plate4 = false;

			ServedAtOne = -1;
			ServedAtTwo = -1;
			ServedAtThree= -1;
			ServedAtFour = -1;

			OrderedAtOne = 0;
			OrderedAtTwo = 0;
			OrderedAtThree= 0;
			OrderedAtFour  = 0;


			move1 = false;
			move2 = false;
			move3 = false;
			move4  = false;

			Lives = 3;
			GameOver = false;
			IsPaused= false;

			Scores = 0;
			Levels = 1;
		}