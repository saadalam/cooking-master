﻿#pragma strict

public var btnType : int;
private var detected : boolean  = false;

public var PauseDialog : GameObject;

function Start () {
}

function Update () {
//if (Input.GetMouseButton(0) && Application.platform != RuntimePlatform.Android)
	if(Input.touchCount == 1)
	{
//	    var wp : Vector3 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		var wp : Vector3 = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
		var touchPos : Vector2 = new Vector2(wp.x, wp.y);
		if(Input.GetTouch(0).phase == TouchPhase.Began && !detected)
		{
		if (GetComponent.<Collider2D>() == Physics2D.OverlapPoint(touchPos))
			{
				if(btnType == 0) //pause
				{
				if(AppController.isMusic) {
					AppController.ClickSound();
				}
				Time.timeScale = 0;
				PauseDialog.SetActive(true);
				AppController.IsPaused = true;
				}
				else if(btnType == 1) //continue
				{
				if(AppController.isMusic) {
					AppController.ClickSound();
				}
				Time.timeScale = 1;
				PauseDialog.SetActive(false);
				AppController.IsPaused = false;
				}
				else if(btnType == 2) //Retry
				{
				if(AppController.isMusic) {
					AppController.ClickSound();
				}
				AppController.Reset();
				Application.LoadLevel(2);
				Time.timeScale = 1;
				}
				else if(btnType == 3) //BacktoMain
				{
				if(AppController.isMusic) {
					AppController.ClickSound();
				}
				AppController.Reset();
				var gameMusic : GameObject = GameObject.Find("MainMenuSound");
                if (gameMusic) {
			    // kill menu music
			    GamePlaySound.AudioBegin = false;
			    Destroy(gameMusic);
					}
				Application.LoadLevel(1);
				Time.timeScale = 1;
				}
				
			}
			detected = true;
		}
     if((Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled) && detected)
	    {
		detected = false;
	    }
	}
}


