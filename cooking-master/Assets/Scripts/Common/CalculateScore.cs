﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CalculateScore : MonoBehaviour {

	public int type;

	// Use this for initialization
	void Start () {
//		PlayerPrefs.SetInt("Score",0);
		if(type == 1)
		{
			AppController.CurrentScore = AppController.Scores ;
			GetComponent<Text>().text = AppController.CurrentScore.ToString();
		}
		else if(type == 2)
		{
			AppController.HighScore = PlayerPrefs.GetInt("Score");
			AppController.CurrentScore = AppController.Scores ;

			if(AppController.CurrentScore >= AppController.HighScore)
			{
				AppController.HighScore = AppController.CurrentScore;
				PlayerPrefs.SetInt("Score",AppController.HighScore);
			}
			GetComponent<Text>().text = AppController.HighScore.ToString();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
