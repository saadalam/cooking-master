﻿#pragma strict

public var btnType : int;
public var lvlNumber : int ;
private var detected : boolean  = false;
public var changeSprite : Sprite;
	
private var spriteChange : SpriteRenderer;
private var defaultSprite : Sprite;
private var soundOn : boolean = true;
private var musicOn: boolean = true;

function Awake ()
{
	spriteChange = GetComponent(SpriteRenderer);
}

function Start () {
	defaultSprite = spriteChange.sprite;
}

function Update()
{
//if (Input.GetMouseButton(0) && Application.platform != RuntimePlatform.Android)
if(Input.touchCount == 1)
{
//	var wp : Vector3 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
	var wp : Vector3 = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
	var touchPos : Vector2 = new Vector2(wp.x, wp.y);
	if(Input.GetTouch(0).phase == TouchPhase.Began && !detected)
	{
	if (GetComponent.<Collider2D>() == Physics2D.OverlapPoint(touchPos))
		{
			if(btnType == 1)	//Play
			 {
			      if(AppController.isMusic) {
					AppController.ClickSound();
			     }
			     
				Application.LoadLevel(2);

			  }
			  
			   else if(btnType == 2)
			 {
			      if(AppController.isMusic) {
					AppController.ClickSound();
			     }
				Application.OpenURL("market://details?id=com.Game4Free"); //More

			  }	
			    
			  else if(btnType == 3)
			 {
			      if(AppController.isMusic) {
					AppController.ClickSound();
			     }
				Application.OpenURL("market://details?id=com.Games4Free.CookingMaster"); //rateus

			  }
			  
			else if(btnType == 4) //LeaderBoard
			 {
			    if(AppController.isMusic) {
					AppController.ClickSound();
			    }
			 }
			 
			   else if(btnType == 5)
			    {
			     if(lvlNumber == 0)
			     {
				  	if(musicOn)
					 {
					  if(AppController.isMusic) {
					AppController.ClickSound();
			    	 }
	                 spriteChange.sprite = changeSprite;
					 musicOn = false;
					 AppController.isMusic = false;
	                 }
	                 else
	                 {
		                  if(AppController.isMusic) {
							AppController.ClickSound();
				     		}
		                 spriteChange.sprite = defaultSprite;
						 musicOn = true;
						 AppController.isMusic = true;
	                 	}
	                 }
	             }  

		  }
		detected = true;
		}
	
	
	if((Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled) && detected)
	{
		detected = false;
	}
}
}
