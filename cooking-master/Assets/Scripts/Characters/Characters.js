﻿#pragma strict
public var isIdle : boolean = false;
public var Lose : boolean = false;

public var Pops : GameObject[];

public static var ItemsOne : GameObject[];
public static var ItemsTwo : GameObject[];
public static var ItemsThree : GameObject[];
public static var ItemsFour : GameObject[];

private var Anim : Animator;

function OnEnable () {
Anim = GetComponent(Animator);
Anim.speed = 0;
}

function Start () {

	if(!AppController.IsFalse)
		{
		ItemsOne = GameObject.FindGameObjectsWithTag("ItemsOne");
		ItemsTwo = GameObject.FindGameObjectsWithTag("ItemsTwo");
		ItemsThree = GameObject.FindGameObjectsWithTag("ItemsThree");
		ItemsFour = GameObject.FindGameObjectsWithTag("ItemsFour");
		PopEnd(ItemsOne);
		PopEnd(ItemsTwo);
		PopEnd(ItemsThree);
		PopEnd(ItemsFour);
		PopEnd(Pops);
		AppController.IsFalse = true;
		}
		
}

function Update () {
	
if(!Lose){
	if((transform.position.x <= AppController.firstPosition) && (transform.position.x > AppController.secondPosition+2) && !AppController.position1)
	{
	Anim = GetComponent(Animator);
	Anim.speed = 1;
		AppController.position1 = true;
		Pops[0].SetActive(true);
		gameObject.name = "p1";
		isIdle = true;
	}
	else if((transform.position.x <= AppController.secondPosition) && (transform.position.x > AppController.thirdPosition+2) && !AppController.position2)
	{
		Anim = GetComponent(Animator);
	    Anim.speed = 1;
		AppController.position2 = true;
		Pops[1].SetActive(true);
		gameObject.name = "p2";
		isIdle = true;
	}
	else if((transform.position.x <= AppController.thirdPosition) && (transform.position.x > AppController.forthPosition+2) && !AppController.position3)
	{
	    Anim = GetComponent(Animator);
		Anim.speed = 1;
		AppController.position3 = true;
		Pops[2].SetActive(true);
		gameObject.name = "p3";
		isIdle = true;
	}
	else if((transform.position.x <= AppController.forthPosition) && !AppController.position4)
	{
		Anim = GetComponent(Animator);
		Anim.speed = 1;
		AppController.position4 = true;
		gameObject.name = "p4";
		Pops[3].SetActive(true);
		isIdle = true;
	}
}
//Character left the shop
	if(transform.position.x <= -15.3)
	{
	Lose = false;
//	CharactersController.SelectCharacter();
	  gameObject.SetActive(false);
	  transform.position.x = 0;
	}

	//if not moving
	if(!isIdle)
	{transform.position.x -= 3 * Time.deltaTime;} //move
	
	
	
	if(AppController.move1 && gameObject.name == "p1")
	{
//	Debug.Log(AppController.move1);
	  StartMovement();
	  AppController.ServedAtOne =-1;
	  AppController.move1 = false;
	}
	if(AppController.move2 && gameObject.name == "p2")
	{
	  StartMovement();
	  AppController.ServedAtTwo =-1;
	  AppController.move2 = false;
	  
	}
	if(AppController.move3  && gameObject.name == "p3")
	{
	  StartMovement();
	  AppController.ServedAtThree =-1;
	  AppController.move3 = false;
	}
	if(AppController.move4  && gameObject.name == "p4")
	{
	  StartMovement();
	  AppController.ServedAtFour =-1;
	  AppController.move4 = false;
	}
}

static function PopEnd(Items: GameObject[])
	{
	  for(var i = 0; i < Items.Length; i++)
	  	{
	  	  Items[i].SetActive(false);
	  	}
	}

function OnAnimEnd()
	{
	if(AppController.isMusic) {
				GetComponent(AudioSource).Play();
				}
	if(gameObject.name == "p1" )
	{
	  AppController.ServedAtOne =-1;
	}
	else if(gameObject.name == "p2" )
	{
	  AppController.ServedAtTwo =-1;
	}
	else if(gameObject.name == "p3" )
	{
	  AppController.ServedAtThree =-1;
	}
	else if(gameObject.name == "p4" )
	{
	  AppController.ServedAtFour =-1;
	}
		StartMovement();
		AppController.Lives -= 1;
	}
	
function Checker(Orders : GameObject[]) // check for all orders served or not
	{
	  for(var i = 0; i < Orders.Length; i++)
	  	{
	  	  if(Orders[i].activeInHierarchy)
	  	  {
	  	  	if(Orders[i].name != "Single" || Orders[i].name != "Double" || Orders[i].name != "Triple")
	  	  		{
	  	  		   Checker(Orders);
	  	  		}
	  	  }
	  	  else
	  	  {
	  	    StartMovement();
	  	  }
	  	}
	}
	
function StartMovement()
	{
	Debug.Log(gameObject.name);
	 if(gameObject.name == "p1" )
	{
		Debug.Log("P1 moving");
		gameObject.name = "1";
		PopEnd(ItemsOne);
		Pops[0].SetActive(false);
		AppController.position1 = false;
	}
	else if(gameObject.name == "p2")
	{
		gameObject.name = "2";
		PopEnd(ItemsTwo);
		Pops[1].SetActive(false);
		AppController.position2 = false;
	}
	else if(gameObject.name == "p3")
	{
		gameObject.name = "3";
		PopEnd(ItemsThree);
		Pops[2].SetActive(false);
		AppController.position3 = false;
	}
	else if(gameObject.name == "p4")
	{
		gameObject.name = "4";
		PopEnd(ItemsFour);
		Pops[3].SetActive(false);
		AppController.position4 = false;
	}
	else
	{
		
	}
	  isIdle = false;
	  Lose = true;
	}
	
