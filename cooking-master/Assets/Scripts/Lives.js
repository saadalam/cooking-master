﻿#pragma strict

public var lives : GameObject[];
public var GameOver : GameObject;
public var character : GameObject;
public var Shapes : GameObject;

function Start () {

}

function Update () {
	if(AppController.Lives == 2)
	{
	  lives[0].SetActive(true);
	}
	else if(AppController.Lives == 1)
	{
	  lives[1].SetActive(true);
	}
	else if(AppController.Lives == 0)
	{
	  lives[2].SetActive(true);
	  GameOver.SetActive(true);
	  character.SetActive(false);
	  Shapes.SetActive(false);
	  Time.timeScale = 0;
	  AppController.IsPaused = true;
	}
}