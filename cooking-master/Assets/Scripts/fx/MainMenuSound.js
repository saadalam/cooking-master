﻿#pragma strict

public var SoundClip : AudioClip;
public static var SoundSource : AudioSource;

function Awake()
{
	AppController.isMusic = true;
	AppController.isSound = true;
	SoundSource = gameObject.AddComponent(AudioSource);
	SoundSource.loop = true;
	SoundSource.clip = SoundClip;
	if(AppController.isMusic) {
		SoundSource.Play(); 
	}
}

function Start () {

}

function Update () {

}

static function StopSound () {
	SoundSource.Stop(); 
}

static function PlaySound () {
	SoundSource.Play(); 
}