﻿#pragma strict
	
static var AudioBegin   : boolean;
public var SoundClip : AudioClip;
private var SoundSource : AudioSource;

function Awake()
{
	if(AppController.isSound) {
	if (!AudioBegin)
	{
		DontDestroyOnLoad(gameObject);
		SoundSource = gameObject.AddComponent(AudioSource);
		SoundSource.loop = true;
		SoundSource.clip = SoundClip;
		SoundSource.volume = 0.7;
		SoundSource.Play(); 
		AudioBegin = true;
	}
	}
}

function Start () {

}

function Update () {

}