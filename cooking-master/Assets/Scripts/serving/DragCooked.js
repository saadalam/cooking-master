﻿#pragma strict

private var detected : boolean  = false;
private var initialPosition : Vector3;
private var offset : Vector2;

public var Indications : GameObject[];
public var RespectiveItems : GameObject[];
public var Parent : GameObject;

private var Anim : Animator;

function Start () {
Anim = gameObject.GetComponentInParent(Animator);
   initialPosition = transform.position;
}

function Update () {

//	if (Input.GetMouseButton(0) && Application.platform != RuntimePlatform.Android)
	if(Input.touchCount == 1)
		{
		
//			var wp : Vector3 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			var wp : Vector3 = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
			var touchPos : Vector2 = new Vector2(wp.x, wp.y);
			if(Input.GetTouch(0).phase == TouchPhase.Began && !detected )
			{
				if (GetComponent.<Collider2D>() == Physics2D.OverlapPoint(touchPos) && !AppController.IsPaused )
				{
					if(AppController.isMusic) {
						AppController.ClickSound();
					}
					offset = new Vector2(transform.position.x - touchPos.x , transform.position.y - touchPos.y);
					detected = true;
				}
			}
			if(detected)
			{
			Anim.speed = 0;
			transform.position = new Vector3(touchPos.x + offset.x,touchPos.y + offset.y,transform.position.z);
			if(!AppController.plate1)
			{
			  Indications[0].SetActive(true);
			}
			if(!AppController.plate2)
			{
			  Indications[1].SetActive(true);
			}
			if(!AppController.plate3)
			{
			  Indications[2].SetActive(true);
			}
			if(!AppController.plate4)
			{
			  Indications[3].SetActive(true);
			}
			
			}
	if((Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled) && detected)
		{
		    if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectiveItems[0].GetComponent.<Renderer>().bounds) && !AppController.plate1) 
			  {
			    AppController.plate1 = true;
			    CheckPan();
			  	RespectiveItems[0].SetActive(true);
			  	transform.position = initialPosition;
			  	Parent.SetActive(false);
			  }
			  else if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectiveItems[1].GetComponent.<Renderer>().bounds) && !AppController.plate2) 
			  {
			    AppController.plate2 = true;
			    CheckPan();
			  	RespectiveItems[1].SetActive(true);
			  	transform.position = initialPosition;
			  	Parent.SetActive(false);
			  }
			  else if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectiveItems[2].GetComponent.<Renderer>().bounds) && !AppController.plate3) 
			  {
			    AppController.plate3 = true;
			    CheckPan();
			  	RespectiveItems[2].SetActive(true);
			  	transform.position = initialPosition;
			  	Parent.SetActive(false);
			  }
			  else if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectiveItems[3].GetComponent.<Renderer>().bounds) && !AppController.plate4) 
			  {
			    AppController.plate4 = true;
			    CheckPan();
			  	RespectiveItems[3].SetActive(true);
			  	transform.position = initialPosition;
			  	Parent.SetActive(false);
			  }
			  else if (gameObject.GetComponent.<Renderer>().bounds.Intersects(CharactersController.dustBin.GetComponent.<Renderer>().bounds)) 
			  {
			    CheckPan();
			  	transform.position = initialPosition;
			  	Parent.SetActive(false);
			  }
			    Anim.speed = 1;
			  for(var i = 0; i < Indications.Length; i++)
			  {
			    Indications[i].SetActive(false);
			  }
				 detected = false;
				 Anim.speed = 1;
				 transform.position = initialPosition;
		 }

					
				
}
}

//function OnMouseUp()
//		{
//		    if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectiveItems[0].GetComponent.<Renderer>().bounds) && !AppController.plate1) 
//			  {
//			    AppController.plate1 = true;
//			    CheckPan();
//			  	RespectiveItems[0].SetActive(true);
//			  	transform.position = initialPosition;
//			  	Parent.SetActive(false);
//			  }
//			  else if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectiveItems[1].GetComponent.<Renderer>().bounds) && !AppController.plate2) 
//			  {
//			    AppController.plate2 = true;
//			    CheckPan();
//			  	RespectiveItems[1].SetActive(true);
//			  	transform.position = initialPosition;
//			  	Parent.SetActive(false);
//			  }
//			  else if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectiveItems[2].GetComponent.<Renderer>().bounds) && !AppController.plate3) 
//			  {
//			    AppController.plate3 = true;
//			    CheckPan();
//			  	RespectiveItems[2].SetActive(true);
//			  	transform.position = initialPosition;
//			  	Parent.SetActive(false);
//			  }
//			  else if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectiveItems[3].GetComponent.<Renderer>().bounds) && !AppController.plate4) 
//			  {
//			    AppController.plate4 = true;
//			    CheckPan();
//			  	RespectiveItems[3].SetActive(true);
//			  	transform.position = initialPosition;
//			  	Parent.SetActive(false);
//			  }
//			  else if (gameObject.GetComponent.<Renderer>().bounds.Intersects(CharactersController.dustBin.GetComponent.<Renderer>().bounds)) 
//			  {
//			    CheckPan();
//			  	transform.position = initialPosition;
//			  	Parent.SetActive(false);
//			  }
//			    Anim.speed = 1;
//			  for(var i = 0; i < Indications.Length; i++)
//			  {
//			    Indications[i].SetActive(false);
//			  }
//				 detected = false;
//				 Anim.speed = 1;
//				 transform.position = initialPosition;
//		 }
function CheckPan()
	{
	  if(gameObject.tag == "pan1")
	  {
	    AppController.pan1 = false;
	  }
	  else if(gameObject.tag == "pan2")
	  {
	    AppController.pan2 = false;
	  }
	  else if(gameObject.tag == "pan3")
	  {
	    AppController.pan3 = false;
	  }
	  else if(gameObject.tag == "pan4")
	  {
	    AppController.pan4 = false;
	  }
	}
