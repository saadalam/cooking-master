﻿#pragma strict

private var detected : boolean  = false;
private var initialPosition : Vector3;
private var offset : Vector2;

public var Indications : GameObject[];
public var RespectiveItems : GameObject[];

function Start () {
   initialPosition = transform.position;
}

function Update () {

//	if (Input.GetMouseButton(0) && Application.platform != RuntimePlatform.Android)
	if(Input.touchCount == 1)
		{
		
//			var wp : Vector3 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			var wp : Vector3 = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
			var touchPos : Vector2 = new Vector2(wp.x, wp.y);
			if(Input.GetTouch(0).phase == TouchPhase.Began && !detected )
			{
				if (GetComponent.<Collider2D>() == Physics2D.OverlapPoint(touchPos)  && !AppController.IsPaused)
				{
					if(AppController.isMusic) {
						AppController.ClickSound();
					}
					offset = new Vector2(transform.position.x - touchPos.x , transform.position.y - touchPos.y);
					detected = true;
				}
			}
			if(detected)
			{
			transform.position = new Vector3(touchPos.x + offset.x,touchPos.y + offset.y,transform.position.z);
			if(!AppController.plate1)
			{
			  Indications[0].SetActive(true);
			}
			if(!AppController.plate2)
			{
			  Indications[1].SetActive(true);
			}
			if(!AppController.plate3)
			{
			  Indications[2].SetActive(true);
			}
			if(!AppController.plate4)
			{
			  Indications[3].SetActive(true);
			}
			
			}
	if((Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled) && detected)
		{
		    if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectiveItems[0].GetComponent.<Renderer>().bounds) && !AppController.plate1) 
			  {
			    AppController.plate1 = true;
			  	RespectiveItems[0].SetActive(true);
			  	transform.position = initialPosition;
			  }
			  else if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectiveItems[1].GetComponent.<Renderer>().bounds) && !AppController.plate2) 
			  {
			    AppController.plate2 = true;
			  	RespectiveItems[1].SetActive(true);
			  	transform.position = initialPosition;
			  }
			  else if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectiveItems[2].GetComponent.<Renderer>().bounds) && !AppController.plate3) 
			  {
			    AppController.plate3 = true;
			  	RespectiveItems[2].SetActive(true);
			  	transform.position = initialPosition;
			  }
			  else if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectiveItems[3].GetComponent.<Renderer>().bounds) && !AppController.plate4) 
			  {
			    AppController.plate4 = true;
			  	RespectiveItems[3].SetActive(true);
			  	transform.position = initialPosition;
			  }
			  for(var i = 0; i < Indications.Length; i++)
			  {
			    Indications[i].SetActive(false);
			  }
				 detected = false;
				 transform.position = initialPosition;
		 }
					
				
}
}

//function OnMouseUp()
//		{
//		    if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectiveItems[0].GetComponent.<Renderer>().bounds) && !AppController.plate1) 
//			  {
//			    AppController.plate1 = true;
//			  	RespectiveItems[0].SetActive(true);
//			  	transform.position = initialPosition;
//			  }
//			  else if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectiveItems[1].GetComponent.<Renderer>().bounds) && !AppController.plate2) 
//			  {
//			    AppController.plate2 = true;
//			  	RespectiveItems[1].SetActive(true);
//			  	transform.position = initialPosition;
//			  }
//			  else if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectiveItems[2].GetComponent.<Renderer>().bounds) && !AppController.plate3) 
//			  {
//			    AppController.plate3 = true;
//			  	RespectiveItems[2].SetActive(true);
//			  	transform.position = initialPosition;
//			  }
//			  else if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectiveItems[3].GetComponent.<Renderer>().bounds) && !AppController.plate4) 
//			  {
//			    AppController.plate4 = true;
//			  	RespectiveItems[3].SetActive(true);
//			  	transform.position = initialPosition;
//			  }
//			  for(var i = 0; i < Indications.Length; i++)
//			  {
//			    Indications[i].SetActive(false);
//			  }
//				 detected = false;
//				 transform.position = initialPosition;
//		 }
