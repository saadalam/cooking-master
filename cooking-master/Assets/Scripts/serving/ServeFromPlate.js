﻿#pragma strict

private var detected : boolean  = false;
private var initialPosition : Vector3;
private var offset : Vector2;

public var RespectivePosition : GameObject[];


function Start () {
   initialPosition = transform.position;
}

function Update () {

//	if (Input.GetMouseButton(0) && Application.platform != RuntimePlatform.Android)
	if(Input.touchCount == 1)
		{
		
//			var wp : Vector3 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			var wp : Vector3 = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
			var touchPos : Vector2 = new Vector2(wp.x, wp.y);
			if(Input.GetTouch(0).phase == TouchPhase.Began && !detected )
			{
				if (GetComponent.<Collider2D>() == Physics2D.OverlapPoint(touchPos) && !AppController.IsPaused)
				{
					if(AppController.isMusic) {
						AppController.ClickSound();
					}
					offset = new Vector2(transform.position.x - touchPos.x , transform.position.y - touchPos.y);
					detected = true;
				}
			}
			if(detected)
			{
			transform.position = new Vector3(touchPos.x + offset.x,touchPos.y + offset.y,transform.position.z);
			
			}
	if((Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled) && detected)
		{
		    if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectivePosition[0].GetComponent.<Renderer>().bounds)) 
			  {
			  
			   if(VerifyObject(Characters.ItemsOne))
			   	{
			   	Scoring();
			   	  AppController.ServedAtOne++;
			   	  Debug.Log("Order = "+AppController.OrderedAtOne +"  Sevred "+ AppController.ServedAtOne);
			   	  if(AppController.OrderedAtOne == AppController.ServedAtOne)
					{
					  AppController.move1 =true;
					}
			   	}
			  }
			  else if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectivePosition[1].GetComponent.<Renderer>().bounds)) 
			  {
			  
			   if( VerifyObject(Characters.ItemsTwo))
			   	{
			   	Scoring();
			   	  AppController.ServedAtTwo++;
			   	  if(AppController.OrderedAtTwo == AppController.ServedAtTwo)
					{
					  AppController.move2 =true;
					}
			   	}
			  }
			  else if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectivePosition[2].GetComponent.<Renderer>().bounds)) 
			  {
			  
			    if(VerifyObject(Characters.ItemsThree))
			   	{
			   	Scoring();
			   	  AppController.ServedAtThree++;
			   	  if(AppController.OrderedAtThree == AppController.ServedAtThree)
					{
					  AppController.move3 =true;
					}
			   	}
			  }
			  else if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectivePosition[3].GetComponent.<Renderer>().bounds)) 
			  {
			  
			   if(VerifyObject(Characters.ItemsFour))
			   	{
			   	Scoring();
			   	  AppController.ServedAtFour++;
			   	  if(AppController.OrderedAtFour == AppController.ServedAtFour)
					{
					  AppController.move4 =true;
					}
			   	}
			  }
		   else if (gameObject.GetComponent.<Renderer>().bounds.Intersects(CharactersController.dustBin.GetComponent.<Renderer>().bounds)) 
			  {
			    CheckPlate();
			    transform.position = initialPosition;
			    gameObject.SetActive(false);
			  }
			  	 transform.position = initialPosition;
				 detected = false;
		 }

					
				
}
}

//
//function OnMouseUp()
//		{
//		    if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectivePosition[0].GetComponent.<Renderer>().bounds)) 
//			  {
//			  
//			   if(VerifyObject(Characters.ItemsOne))
//			   	{
//			   	Scoring();
//			   	  AppController.ServedAtOne++;
//			   	  Debug.Log("Order = "+AppController.OrderedAtOne +"  Sevred "+ AppController.ServedAtOne);
//			   	  if(AppController.OrderedAtOne == AppController.ServedAtOne)
//					{
//					  AppController.move1 =true;
//					}
//			   	}
//			  }
//			  else if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectivePosition[1].GetComponent.<Renderer>().bounds)) 
//			  {
//			  
//			   if( VerifyObject(Characters.ItemsTwo))
//			   	{
//			   	Scoring();
//			   	  AppController.ServedAtTwo++;
//			   	  if(AppController.OrderedAtTwo == AppController.ServedAtTwo)
//					{
//					  AppController.move2 =true;
//					}
//			   	}
//			  }
//			  else if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectivePosition[2].GetComponent.<Renderer>().bounds)) 
//			  {
//			  
//			    if(VerifyObject(Characters.ItemsThree))
//			   	{
//			   	Scoring();
//			   	  AppController.ServedAtThree++;
//			   	  if(AppController.OrderedAtThree == AppController.ServedAtThree)
//					{
//					  AppController.move3 =true;
//					}
//			   	}
//			  }
//			  else if (gameObject.GetComponent.<Renderer>().bounds.Intersects(RespectivePosition[3].GetComponent.<Renderer>().bounds)) 
//			  {
//			  
//			   if(VerifyObject(Characters.ItemsFour))
//			   	{
//			   	Scoring();
//			   	  AppController.ServedAtFour++;
//			   	  if(AppController.OrderedAtFour == AppController.ServedAtFour)
//					{
//					  AppController.move4 =true;
//					}
//			   	}
//			  }
//		   else if (gameObject.GetComponent.<Renderer>().bounds.Intersects(CharactersController.dustBin.GetComponent.<Renderer>().bounds)) 
//			  {
//			    CheckPlate();
//			    transform.position = initialPosition;
//			    gameObject.SetActive(false);
//			  }
//			  	 transform.position = initialPosition;
//				 detected = false;
//		 }
function CheckPlate() // to enable plate for other objects
	{
	  if(gameObject.tag == "plate1")
	  {
	    AppController.plate1 = false;
	  }
	  else if(gameObject.tag == "plate2")
	  {
	    AppController.plate2 = false;
	  }
	  else if(gameObject.tag == "plate3")
	  {
	    AppController.plate3 = false;
	  }
	  else if(gameObject.tag == "plate4")
	  {
	    AppController.plate4 = false;
	  }
	}
	
function VerifyObject(Items : GameObject[]) : boolean // verify weather serving object is same as ordered or not
	{
	  for(var i = 0; i < Items.Length; i++)
	  	{
	  	 if(Items[i].activeInHierarchy)
	  	 	{
		  	  if(gameObject.name == Items[i].name) //rightServed
		  	  	{
		  	  	  Items[i].SetActive(false);
		  	  	  transform.position = initialPosition;
		  	  	  gameObject.SetActive(false);
		  	  	  CheckPlate();
		  	  	  return true;
		  	  	}
		  	}
		  	else
		  	{
		  	 
		  	} 	
	  	}
	}

function Scoring()
	{
	  if(gameObject.name == 3)
	  {
	    AppController.Scores += 40;
	  }
	  else
	  {
	    AppController.Scores += 100;
	  }
	  AppController.Levels = (AppController.Scores / 500);
//	  if(AppController.Scores != 0 && (AppController.Scores % 300 == 0))
//		{
//	 	  AppController.Levels += 1;
//	    }
	}
	

