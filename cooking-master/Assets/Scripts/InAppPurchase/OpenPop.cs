﻿using UnityEngine;
using System.Collections;


public class OpenPop : MonoBehaviour 
{
	private bool detected = false;
	public GameObject respective;
	public GameObject[] parent;

	public int Type;
	// Use this for initialization
	void Start () {
		
		
	}
	
	// Update is called once per frame
	void Update () {
//		if (Input.GetMouseButton (0) && Application.platform != RuntimePlatform.Android)
		if (Input.touchCount == 1) 
		{
//			Vector3 wp  = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector3 wp = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position);
			Vector2 touchPos = new Vector2 (wp.x, wp.y);
			
		if (Input.GetTouch (0).phase == TouchPhase.Began && !detected) 
		{
			if (GetComponent<Collider2D> () == Physics2D.OverlapPoint (touchPos) && !detected  ) 
			{
					if(Type == 0)
					{
					for(int i = 0 ; i< parent.Length ; i++)
					{
						parent[i].SetActive(false);
					}
				respective.SetActive(true);
					
					}
				
			}
		}

		if((Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled) && detected)
		{
			detected = false;
		}
		}
//		if(Input.GetMouseButtonUp(0))
//		{
//			detected = false;
//		}
	}
	public void NoAdsBtn()
	{
		for(int i = 0 ; i< parent.Length ; i++)
		{
			parent[i].SetActive(true);
		}
		respective.SetActive (false);
		
	}
	public void Coins2000(GameObject respective)
	{
		respective.SetActive (false);
		
	}
	public void Coins5000(GameObject respective)
	{
		respective.SetActive (false);
		
	}
	public void Coins8000(GameObject respective)
	{
		respective.SetActive (false);
		
	}
	public void Coins15000(GameObject respective)
	{
		
		respective.SetActive (false);
		
	}
	
	public void Close()
	{
		for(int i = 0 ; i< parent.Length ; i++)
		{
			parent[i].SetActive(true);
		}
		respective.SetActive (false);
//		AppController.PopActive = false;
	}
	
	public void ShowPop(GameObject respective)
	{
		respective.SetActive (true);
	}
}
