﻿using UnityEngine;
using System.Collections;

public class StartAnim : MonoBehaviour {
	public GameObject Anim;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(AppController.PurchaseDone)
		{
			Anim.SetActive(true);
			AppController.PurchaseDone = false;
		}
	}
}
