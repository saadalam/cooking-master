﻿#pragma strict

public var SubObjects : GameObject[];
public var ObjType : int;

function OnEnable () {
	var r;
	if(ObjType == 11 || ObjType == 22 || ObjType == 33 || ObjType == 44)
	{
		if(AppController.Levels <= 3)
			{
			  r = 0;
			}
		else if(AppController.Levels < 6 && AppController.Levels > 3)
			{
			  r = Random.Range(0,2);
			}
		else
			{
				r = Random.Range(0,3);
			}
	}
	else
	{
		r = Random.Range(0,3);
	}
		
		
	if(ObjType == 11)
	{
	  AppController.OrderedAtOne = r;
	}
	else if(ObjType == 22)
	{
	  AppController.OrderedAtTwo = r;
	}
	else if(ObjType == 33)
	{
	  AppController.OrderedAtThree = r;
	}
	else if(ObjType == 44)
	{
	  AppController.OrderedAtFour = r;
	}
	
	SubObjects[r].SetActive(true);
	if(ObjType > 1 && ObjType < 5)
	{
	  for(var i = 0; i < SubObjects.Length; i++)
	  {
	    if(!SubObjects[i].activeInHierarchy)
	    	{
	    	  SubObjects[i].SetActive(true);
	    	  break;
	    	}
	  }
	}
	if(ObjType == 3)
	{
	  for(var j = 0; j < SubObjects.Length; j++)
	  {
	    if(!SubObjects[j].activeInHierarchy)
	    	{
	    	  SubObjects[j].SetActive(true);
	    	  break;
	    	}
	  }
	}
}

function Update () {

}